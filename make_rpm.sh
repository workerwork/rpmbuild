#!/bin/bash -

function installsoftware() {
    rpm -qa | grep epel-release
    [[ $? == 1 ]] && yum install -y epel-release
    rpm -qa | egrep rpm-build-[0-9]+
    [[ $? == 1 ]] && yum install -y rpm-build
    rpm -qa | grep rpm-sign
    [[ $? == 1 ]] && yum install -y rpm-sign
    #rpm -qa | grep ^git
    #[[ $? == 1 ]] && yum install -y git
    rpm -qa | grep ^gnupg
    [[ $? == 1 ]] && yum install -y gnupg
    rpm -qa | grep ^expect 
    [[ $? == 1 ]] && yum install -y expect
}

function setrpmmacro() {
    [[ ! ~/.rpmmacros ]] && touch ~/.rpmmacros
    [[ $(grep "%_signature" ~/.rpmmacros) ]] && \
        sed -i "s/.*\(%_signature\).*/\1 gpg/" ~/.rpmmacros || \
        echo "%_signature gpg" >> ~/.rpmmacros
    [[ $(grep "%_gpg_path" ~/.rpmmacros) ]] && \
        sed -i "s/.*\(%_gpg_path\).*/\1 ~\/.gnupg/" ~/.rpmmacros || \
        echo "%_gpg_path ~/.gnupg" >> ~/.rpmmacros
    if [[ ! $1 ]];then
        [[ $(grep "%_gpg_name" ~/.rpmmacros) ]] && \
            sed -i "s/.*\(%_gpg_name\).*/\1 baicells <baicells@baicells.com>/" ~/.rpmmacros || \
            echo "%_gpg_name baicells <baicells@baicells.com>" >> ~/.rpmmacros
    elif [[ $1 ]];then
        [[ $(grep "%_gpg_name" ~/.rpmmacros) ]] && \
            sed -i "s/.*\(%_gpg_name\).*/\1 $1/" ~/.rpmmacros || \
            echo "%_gpg_name $1" >> ~/.rpmmacros
    fi
    [[ $(grep "%_gpgbin" ~/.rpmmacros) ]] && \
            sed -i "s@.*\(%_gpgbin\).*@\1 $(which gpg)@" ~/.rpmmacros || \
            echo "%_gpgbin $(which gpg)" >> ~/.rpmmacros
}

function makerpm_egw() {
    name=$(awk '/Name:/{print $2}' SPECS/egw.spec)
    version=$(awk '/Version:/{print $2}' SPECS/egw.spec)
    release=$(awk '/Release/{print $2}' SPECS/egw.spec |awk -F '%' '{print $1}')
    cd SOURCES
    cp -rf eGW-install $name-$version
    tar -zcvf $name-$version.tar.gz $name-$version
    rm -rf $name-$version
    cd ..
    rpmbuild -bb SPECS/egw.spec \
        --define "_topdir $(pwd)" \
        --define "debug_package %{nil}" \
        --define "__os_install_post %{nil}"
        #--define "_signature gpg" \
        #--define "_gpg_path /$(whoami)/.gnupg" \
        #--define "_gpg_name baicells <baicells@baicells.com>" \
        #--define "_gpgbin $(which gpg)" \
        #--sign
    mv -f RPMS/x86_64/$name-$version-$release*.rpm .
    rm -f RPMS/x86_64/*.rpm
    rm -f SOURCES/$name-$version.tar.gz
}

function setversion_egw() {
    if [[ ! $1 ]];then
        sed -i "s/\(Version:\).*/\1    1.0.0/" SPECS/egw.spec
    else
        sed -i "s/\(Version:\).*/\1    $1/" SPECS/egw.spec
    fi
}

function setrelease_egw() {
    if [[ ! $1 ]];then
        sed -i "s/\(Release:\).*%/\1    1%/" SPECS/egw.spec
    else
        sed -i "s/\(Release:\).*%/\1    $1%/" SPECS/egw.spec
    fi
}

function setgpgkey() {
    gpg --list-key | grep "baicells <baicells@baicells.com>"
    [[ $? == 1 ]] && gpg --import gpgkey/subkey
}

function addsign() {
    rpmname=$(find . -maxdepth 1 -name "$name-$version-$release*.rpm")
    expect << EOF
    set timeout 5
    #set rpmname [lindex $argv 0]
    spawn rpm --addsign $rpmname
    expect {
        "Enter pass phrase: " {
            send "baicells\r"
        }  
    }
    expect eof;
EOF
}

function init() {
    setrpmmacro
    setversion_egw
    setrelease_egw
    setgpgkey
}

#install software
installsoftware
#init
init

ARGS=`getopt -o hv:s:r: --long help,version,sign:,release: -- "$@"`
if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi
eval set -- "$ARGS"
while true;do
    case "$1" in
        -s|--sign)
            #echo "-s | --sign"
            setrpmmacro "$2"
            shift 2
            ;;
        -r|--release)
            setrelease_egw $2
            shift 2
            ;;
        -v|--version)
            setversion_egw $2
            #echo "version: $2"
            shift 2
            ;;
        -h|--help)
            echo "-h | --help"
            shift
            ;;
        --)
            shift
            break
            ;;
        *) 
            echo "unknown:{$1}"
            exit 1
            ;;
    esac
done

makerpm_egw
addsign


